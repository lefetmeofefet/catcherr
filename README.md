# Catcherr
Did you ever deploy a web app, only to discover that The Grate Safari throws an exception, and you have no idea why?
<br>
Catcherr catches those err's for you.

## Installation

Using npm:
```shell
$ npm install catcherr
```

In a browser, using cdn:
```html
<script src="https://cdn.jsdelivr.net/npm/catcherr/catcherr.js" type="module"></script>
```

## Usage

#### Using es6 imports
```html
<script type="module">
    import catcher from "catcherr.js" // if you dont use a bundler, you can replace with https://cdn.jsdelivr.net/npm/catcherr/catcherr.js
    catcher(error => console.log("Caught global error: ", error))
</script>
```
#### Importing in html
```html
<script src="https://cdn.jsdelivr.net/npm/catcherr/catcherr.js" type="module"></script>
<script>
    window.onError = error => console.log("Caught global error: ", error)
</script>
```

