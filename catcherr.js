let errorCatchers = [];

// Normal errors
window.addEventListener("error", function (e) {
    const errorObject = {
        message: e.error.message,
        stack: e.error.stack,
        isAsync: false
    };
    publishError(errorObject);
    return false;
}, {
    useCapture: true
});

// Async errors
window.addEventListener('unhandledrejection', function (e) {
    const errorObject = {
        message: e.reason.message,
        stack: e.reason.stack,
        isAsync: true,
    };
    publishError(errorObject);
});

function publishError(error) {
    if (errorCatchers.length === 0 && window.onError == null) {
        console.warn("Catcherr: Error was thrown but nobody subscribed: ", error)
    }
    for (let errorCatcher of errorCatchers) {
        errorCatcher(error)
    }
    window.onError && window.onError(error);
}

function subscribe(cb) {
    errorCatchers.push(cb);
}
const catcher = subscribe;
export default catcher;
